<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
<form action="/welcome" method="POST">
    @csrf
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <label for="fname">First name:</label><br>
    <input type="text" id="fname" name="fname"><br>
    <label for="lname">Last name:</label><br>
    <input type="text" id="lname" name="lname">
    <br></br>
    <label for="gender">gender: </label><br>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label>
    <br></br>
    <label for="Nationality">Nationality: <br></label>
    <select id="Nationality" name="Nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="English">English</option>
        <option value="Other">Other</option>
    </select>
    <br></br>
    <label for="Spoken"><br>Language Spoken: </label><br>
    <input type="checkbox" id="indonesian" name="Spoken" value="indonesian">
    <label for="indonesian"> Indonesian</label><br>
    <input type="checkbox" id="english" name="Spoken" value="english">
    <label for="english"> English</label><br>
    <input type="checkbox" id="other1" name="Spoken" value="other1">
    <label for="other1"> Other</label>
    <br></br>
    <label for="Bio">Bio :<br> </label>
    <textarea name="Bio" rows="10" cols="10"></textarea>
    <br></br>
    <input type="submit" value='Sign Up'>
</form>

</body>
</html>