<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/hello-laravel', function () {
    echo "Ini Adalah Halaman baru <br>";
    return 'Hello Laravel';
});
Route::get('/Laravel', function() {
    return view("Laravel");
});
Route::get('/home', 'HomeController@home');
Route::get('/', 'AuthController@form');

Route::post('/welcome', 'AuthController@welcome');

