<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
     public function form(){
         return view('form');
     }
     public function welcome(Request $request){
        $fname=$request->input('fname');
        $lname=$request->input('lname');
        $name=$fname . " " . $lname;
        return view('welcome',['name'=> $name]);
     }
}
